# AppImage Helpers

Simple script to help manage AppImages.

Everything you need is contained in one shell script.  Just install it wherever you want (you can use 'make install' to install it)

Once the setup below is completed and an AppImage is installed with this script, you should be able to run the AppImage by its name like any other command, and should also appear in your desktop environment's menus.

## Setup

By default, the AppImages will be installed in /opt/AppImages if it exits else ~/AppImages.  It will automatically create ~/AppImages if needed.  If you are using /opt/AppImages, you will need to have write access to that directory.  If you wish a different location, modify 'install_dir' at the top of the appimage script.  The rest of this README will assume ~/AppImages is being used.

Add ~/AppImages/bin to your PATH.  You should probably log out and log back in for it to take effect.


## Files modified

This script will create and delete files in ~/AppImages (or /opt/AppImages).

This will also create/delete .desktop files in ~/.local/share/applications

Mime files may be installed under ~/.local/share/

The Makefile may install files in: /usr/local/bin ~/bin ~/.local/bin.




## Using the appimage script

appimage has several subcommands

### install

```
appimage install <appimage-file>
```

Install the given AppImage file.


### uninstall

```
appimage uninstall <appimage>
```

Uninstalls a specific AppImage or all version of a named AppImage

### list

```
appimage list [appimage-name]
```

Lists all installed AppImages or all versions of named AppImage.  The output is preceeded by a '*' if that version is the currently active one.

## version

```
appimage version [appimage-name]
```

Select which version (assuming more then one version of an AppImage is installed) to be the one that gets run.

## Related items

https://gitlab.com/jim_bauer/flatpak-helpers


## License

This is licensed under the GPL version 3.0 (or later).  See LICENSE file.


