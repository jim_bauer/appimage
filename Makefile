# SPDX-License-Identifier: GPL-3.0-or-later


uid	:= $(shell id -u)
pwd	:= $(shell pwd)


# Set install install location
# If root, we'll use /usr/local/bin
# If not-root, use ~/bin if it exists else ~/.local/bin
ifeq ($(uid), 0)
 bindir	= /usr/local/bin
else
 ifneq ($(wildcard $(HOME)/bin/.),)
  bindir	= $(HOME)/bin
 else
  bindir	= $(HOME)/.local/bin
 endif
endif


default:

install:
	install -d $(bindir)
	install -m 555 appimage $(bindir)

# install a symlink back to source tree
sinstall: cant-do-this-as-root
	install -d $(bindir)
	ln -s -f $(pwd)/appimage $(bindir)/

cant-do-this-as-root:
	test $(uid) -ne 0

clean:
	$(RM) *~
